\documentclass[11pt]{amsart}

\usepackage{beton} % Use Concrete font for text
\DeclareFontSeriesDefault[rm]{bf}{sbc}
\usepackage[euler-digits,euler-hat-accent]{eulervm} % Use Euler font for math

\usepackage[margin=1.0in,footskip=0.25in,top=0.75in,left=1.00in,right=1.00in]{geometry}  % Margins

\linespread{1.3} % 1.5 spacing?
\usepackage{fancyhdr} % Header
\setlength{\headheight}{14.5pt}
\pagestyle{fancy}
\fancyfoot{}
\lhead{} % Left header
\rhead{\thepage} % Right header

\usepackage{amsmath,amsfonts,amsthm,amssymb,mathtools} % Math stuff
\usepackage[italicdiff]{physics} % Derivatives
\usepackage{graphicx,caption,subcaption,tikz,pdfpages,tcolorbox,todonotes,enumitem} % Graphics/extras

% === Math shortcuts ===

% Theorems
\newtheorem*{thm}{Theorem}
\newtheorem*{defn}{Definition}
\newtheorem*{ex}{Example}

% Blackboard bold
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\E}{\mathbb{E}}
\renewcommand{\P}{\mathbb{P}}

% Real and complex parts
\renewcommand{\Im}{\mathrm{Im}}
\renewcommand{\Re}{\mathrm{Re}}

% Integral d
\renewcommand{\d}{\;d}

\DeclareMathOperator{\linspan}{span}

% ======================


\begin{document}

\title{Notes on compressive spectral collocation methods, sublinear-time function approximation methods, and how to combine them}
\author{Craig Gross\\\today}
\maketitle

\section{Spectral method preliminaries}%
\label{sec:spectral_method_preliminaries}

\subsection{Series representations and interpolation}%
\label{sub:series_representations_and_interpolation}

Let $ \Omega = (0, 1)^d $.
Choose a multiindexed, orthonormal tensor product basis of $ L^2(\Omega) $, $ \{ \psi_j \}_{ j \in \N_0^d } $.
Then any $ u \in L^2(\Omega) $ can be written
\begin{equation*}
	u = \sum_{ j \in \N^d } \hat u_j \psi_j, \qquad \hat u_j = \langle u, \psi_j \rangle_{ L^2(\Omega) }
\end{equation*}
To make things finite, we can consider the truncation of $ u $ to the cube $ [n]^d $ (here $ [n] = {1, \ldots, n} $; if e.g., $ \psi_j $ is tensor product sine basis, no multiindices with zeros end up contributing to the whole basis).
Defining $ N := n^d $,
\begin{equation*}
	u_N = \sum_{ j \in [n]^d } \hat u_j \psi_j
\end{equation*}
with $ \hat u_j $ as above.
Note that this is the projection of $ u $ to $ \linspan \{ \psi_j \}_{ j \in [n]^d } $.

Alternatively, we can consider an approximation to $ u $ by interpolation.
Supposing that we choose a collocation grid $ \{t_q\}_{ q \in [n]^d } \subset \Omega $ for our basis, we define the interpolant on the full grid
\begin{equation*}
	u_N^\mathrm{full} = \sum_{ j \in [n]^d } \hat u_j^\mathrm{full} \psi_{ j },
\end{equation*}
where the $ \hat u_j^\mathrm{full} $ are determined by the fact that $ u_N(t_q) = u(t_q) $ for all $ q \in [n]^d $.
In a collocation grid determined by gaussian abscissa of the basis, we can write
\begin{equation*}
	\hat u_j^\mathrm{full} = \frac{\sum_{ q \in [n]^d } u(t_q) \psi_j(t_q)}{\sum_{ q \in [n]^d } \psi_j(t_q)^2}.
\end{equation*}

\subsection{Applying to PDE}%
\label{sub:applying_to_pde}

Consider the PDE
\begin{equation*}
	\begin{cases}
		\mathcal{L}u := -\nabla \cdot (\eta \nabla u) = F, &\text{in } \Omega\\
		u = 0 & \text{on } \partial \Omega
	\end{cases}
\end{equation*}

Solving via Galerkin approximation we assume an a priori finite spectral representation of a candidate solution
\begin{equation*}
	\tilde u_N = \sum_{ j \in [n]^d } x_j \psi_j.
\end{equation*}
We now use an orthonormal test basis $ \{\phi_{ j }\}_{ j \in [n]^d } $, and solve for the spectral coefficients by projecting the residual $ \mathcal{L}u - F  $ onto $ \linspan \{\phi_{ j }\}_{ j \in [n]^d } $, that is,
\begin{equation*}
	0 = \langle \mathcal{L}u - F, \phi_{ j } \rangle = \sum_{ k \in [n]^d } x_k \langle \mathcal{L} \psi_k, \phi_j \rangle - \langle F, \phi_j \rangle \quad \text{for all } j \in [n]^d.
\end{equation*}
We then collect these equations into the matrix equation
\begin{equation*}
	Ax = f, \qquad A_{ j, k } = \langle \mathcal{L}\psi_k, \phi_j \rangle, \quad f_j = \langle F, \phi_j \rangle.
\end{equation*}
\todo[inline]{Based on the well-posedness of $ \mathcal{L} $ and the choice of test basis, we can get some sort of reasonable comparison between $ x_{ j } $ and $ \hat u_j $, and therefore $ \tilde u_N $ and $ u_N $ or $ u $.}

Instead of enforcing the residual to be zero on $ \linspan \{\phi_{ j }\}_{ j \in [n]^d } $, we could also enforce that it is zero on a collocation grid, i.e.,
\begin{equation*}
	F(t_q) = \mathcal{L}u_N^\mathrm{full}(t_q) = \sum_{ k \in [n]^d } x_k^\mathrm{full} (\mathcal{L} \psi_k)(t_q), \qquad \textrm{for all } q \in [n]^d.
\end{equation*}
Again, we can rewrite as a matrix equation
\begin{equation}
	\label{eq:SpectralCollocationMatrix}
	Bx^\mathrm{full} = f^\mathrm{full}, \qquad B_{ q, k } = (\mathcal{L} \psi_k)(t_q), \quad f^\mathrm{full}_q = F(t_q).
\end{equation}

\section{General outline for compressive spectral collocation}%
\label{sec:general_outline_for_compressive_spectral_collocation}

Supposing that the solution $ u $ is sparse in the chosen basis $ \{\psi_j\}_{ j \in [n]^d } $, we can think of the system in \eqref{eq:SpectralCollocationMatrix} as a compressive sensing problem, so long as $ B $ satisfies some kind of RIP-like properties.
In particular, with the right basis and differential operator, $ B $ can be orthogonal.
\todo[inline]{This is an extremely important assumption for analysis.
What if we want to choose our basis differently?
Can we make up for deficits regarding the orthogonality of $ B $?
E.g., Riesz bases?}
In this case, the columns of $ B $ form a bounded orthonormal system for $ L^2([N]) $.
Subsampling this bounded orthonormal system with respect to uniform probability measure on $ N $ amounts to subsampling the rows of $ B $ which produces a matrix with the RIP.
The number of columns we keep depends quadratically on the BOS constant of the columns of $ B $.
\todo[inline]{This is likely to be exponential in $ d $.
What options do we have for sidestepping this?}
Applying any method for solving a compressive sensing problem then results in a sparse approximation to $ x^\mathrm{full} $.
\todo[inline]{What kind of method do we want to use?
How do we relate the sparse approximation back to the original solution $ u $.}
\todo[inline]{Is there a way to take better advantage of the direct high-dimensional function approximation techniques, rather than going through the discrete basis first?
	Choosing the collocation grid by random sampling?
	Our function sampling is no longer with respect to an orthonormal basis, but with respect to the operated on basis:
	\begin{equation*}
		F(t) = \sum_{ j \in [n]^d } x_j^\mathrm{full} (\mathcal{L} \phi_j)(t).
	\end{equation*}
	What can we get in this new ``basis''?
	Can we apply the high-dimensional function techniques to this?
}

\section{How to handle non-orthogonal $ B $ }%
\label{sec:how_to_handle_non_orthogonal_B}

The truncated basis used in \cite{brugiapaglia_compressive_2020} is 
\begin{equation*}
	\psi_j(z) := \frac{2^{ d/2 }}{\pi^2 \norm{ j }_2^2 (n + 1)^{ d / 2 }} \prod_{ k = 1 }^d \sin( \pi j_k z_k).
\end{equation*}
When $ \eta \equiv 1 $, we have 
\begin{equation*}
	\mathcal{L} \psi_j(x) = - \Delta \psi_j(x) = \prod_{ k = 1 }^d \sqrt{ \frac{2}{n + 1}  } \sin(\pi j_k z_k).
\end{equation*}
With the equispaced collocation grid
\begin{equation*}
	t_q = \frac{q}{n + 1} \in [0, 1]^d, \quad q \in [n]^d,
\end{equation*}
we obtain the matrix elements
\begin{equation*}
	B_{ q, k } = (\mathcal{L}\psi_k)(t_q) = \prod_{ \ell = 1 }^d \sqrt{ \frac{2}{n + 1} } \sin \left( \frac{\pi k_\ell q_\ell }{n + 1}  \right).
\end{equation*}


\begin{ex}

	Let's work with an example for $ d = 2 $, $ n = 2 $.
	We write out $ [n]^d $ in lexicographic order:
	\begin{equation*}
		[n]^d = \left\{ (1, 1), (1, 2), (2, 1), (2, 2) \right\}.
	\end{equation*}
	Then $ B $ will be a $ 4 \times 4 $ matrix:
	\begin{align*}
		&B=
		\begin{pmatrix}
			B_{(1,1), (1,1)} & B_{(1,1), (1,2)} & B_{(1,1), (2,1)} & B_{(1,1), (2,2)} \\ 
			B_{(1,2), (1,1)} & B_{(1,2), (1,2)} & B_{(1,2), (2,1)} & B_{(1,2), (2,2)} \\ 
			B_{(2,1), (1,1)} & B_{(2,1), (1,2)} & B_{(2,1), (2,1)} & B_{(2,1), (2,2)} \\ 
			B_{(2,2), (1,1)} & B_{(2,2), (1,2)} & B_{(2,2), (2,1)} & B_{(2,2), (2,2)} \\ 
		\end{pmatrix} \\
		  &=
		  \frac{2}{3} \cdot\\
		  &
		\begin{pmatrix}
			\sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right)  \sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right) & \sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right) \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) & \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) \sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right) & \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) \\
			\sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right)  \sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right) & \sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right) \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right) & \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) \sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right) & \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right) \\
			\sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right)  \sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right) & \sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right) \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) & \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right) \sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right) & \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right) \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) \\
			\sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right)  \sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right) & \sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right) \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right) & \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right) \sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right) & \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right) \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right) 
		\end{pmatrix}\\
		  &=
		  \begin{pmatrix}
			  \sqrt{ \frac{2}{3} } \sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right) & \sqrt{ \frac{2}{3} } \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) \\
			  \sqrt{ \frac{2}{3} } \sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right) & \sqrt{ \frac{2}{3} } \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right)
		  \end{pmatrix}
		  \otimes
		  \begin{pmatrix}
			  \sqrt{ \frac{2}{3} } \sin \left( \frac{\pi \cdot 1 \cdot 1}{3} \right) & \sqrt{ \frac{2}{3} } \sin \left( \frac{\pi \cdot 1 \cdot 2}{3} \right) \\
			  \sqrt{ \frac{2}{3} } \sin \left( \frac{\pi \cdot 2 \cdot 1}{3} \right) & \sqrt{ \frac{2}{3} } \sin \left( \frac{\pi \cdot 2 \cdot 2}{3} \right)
		  \end{pmatrix} \\
		  &=: S_2 \otimes S_2,
	\end{align*}
	where $ \otimes $ is the matrix Kronecker product.

	Note that $ S_2 $ is the discrete sine transform matrix and is orthogonal.

\end{ex}

\bibliographystyle{abbrv}
\bibliography{References}


\end{document}
